using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace OfficeTools.Employees.Droid.Classes
{
    public class NavigationDrawerItem
    {
        public string title { get; set; }
        public NavigationDrawerItem()
        {

        }
        public NavigationDrawerItem(String title)
        {
            this.title = title;
        }

    }
}