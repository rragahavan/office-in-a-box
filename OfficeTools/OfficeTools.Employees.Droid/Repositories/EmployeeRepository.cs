using Newtonsoft.Json;
using OfficeTools.Shared;
using RestSharp;
using System;

namespace OfficeTools.Employees.Droid.Repositories
{
    public class EmployeeRepository
    {
        public EmployeeListResponse GetEmployeeList()
        {
            EmployeeListResponse empList;
            var client = new RestClient(new Uri("http://192.168.1.118:3000/"));
            ServiceRequest serviceRequest = new ServiceRequest("hrms", "employeelist");
            var request = new RestRequest("api/invoke", Method.POST);
            request.AddJsonBody(serviceRequest);
            var response = client.Execute(request);
            empList = JsonConvert.DeserializeObject<EmployeeListResponse>(response.Content);
            return empList;
        }

    }
}