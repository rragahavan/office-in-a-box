using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V4.View;
using static Android.Resource;
using V7Toolbar = Android.Support.V7.Widget.Toolbar;
using Com.Nostra13.Universalimageloader.Core;
using OfficeTools.Shared;
using Android.Views.Animations;

namespace OfficeTools.Employees.Droid.Views
{
    [Activity(Theme = "@style/Theme.DesignDemo", Label = "SingleUserDetailsActivity")]
    public class SingleUserDetailsActivity : NavigationDrawerActivity
    {
        TextView toolbarTitle, nameText, employeeIdText, emailAddressText, roleText;
        ImageView profileImage;
        Android.Views.Animations.Animation anim;
        List<Employee> employeeDetails = new List<Employee>();

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.SingleUserDetails);
            var toolbar = FindViewById<V7Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetDisplayShowTitleEnabled(false);
            SupportActionBar.SetHomeButtonEnabled(true);
            SupportActionBar.SetHomeAsUpIndicator(Resource.Drawable.hamburgers);
            toolbarTitle = FindViewById<TextView>(Resource.Id.toolbarTitle);
            toolbarTitle.Text = Resources.GetString(Resource.String.EmployeeDetails);
            set(Resources.GetStringArray(Resource.Array.nav_drawer_items));
            nameText = FindViewById<TextView>(Resource.Id.NameText);
            employeeIdText = FindViewById<TextView>(Resource.Id.employeeIdText);
            roleText = FindViewById<TextView>(Resource.Id.roleText);
            emailAddressText = FindViewById<TextView>(Resource.Id.emailAddressText);
            profileImage = FindViewById<ImageView>(Resource.Id.profileImage);
            var config = ImageLoaderConfiguration.CreateDefault(ApplicationContext);
            ImageLoader.Instance.Init(config);
            string employeeId = Intent.GetStringExtra("employeeId");
            employeeDetails = new Repositories.EmployeeRepository().GetEmployeeList().data;
            var employee = new Repositories.EmployeeRepository().GetEmployeeList().data.
                Find(x => x.empcode == employeeId);
            nameText.Text = employee.firstname;
            roleText.Text = employee.lastname;
            employeeIdText.Text = employee.empcode.ToString();
            emailAddressText.Text = employee.email;
            ImageLoader imageLoader = ImageLoader.Instance;
            imageLoader.DisplayImage("http://192.168.1.118:3000" + employee.profileImage, profileImage);
            anim = AnimationUtils.LoadAnimation(ApplicationContext,
                        Resource.Animation.scale_animation);
            profileImage.StartAnimation(anim);
        }
    }
}