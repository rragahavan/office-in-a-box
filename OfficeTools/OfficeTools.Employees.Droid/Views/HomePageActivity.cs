﻿using Android.App;
using Android.Widget;
using Android.OS;
using OfficeTools.Employees.Droid.Views;
using V7Toolbar = Android.Support.V7.Widget.Toolbar;

namespace OfficeTools.Employees.Droid
{
    [Activity(Theme = "@style/Theme.DesignDemo", Label = "OfficeTools.Employees.Droid", MainLauncher = true, Icon = "@drawable/icon")]
    public class HomePageActivity : NavigationDrawerActivity
    {
        TextView toolbarTitle;
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.Main);
            set(Resources.GetStringArray(Resource.Array.nav_drawer_items));
            var toolbar = FindViewById<V7Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            toolbarTitle = FindViewById<TextView>(Resource.Id.toolbarTitle);
            toolbarTitle.Text = Resources.GetString(Resource.String.Hrms);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetDisplayShowTitleEnabled(false);
            SupportActionBar.SetHomeButtonEnabled(true);
            SupportActionBar.SetHomeAsUpIndicator(Resource.Drawable.hamburgers);
        }
    }
}

