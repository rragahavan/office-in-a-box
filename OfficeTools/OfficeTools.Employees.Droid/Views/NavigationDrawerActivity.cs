using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V7.App;
using Android.Support.V4.Widget;
using OfficeTools.Employees.Droid.Classes;
using OfficeTools.Employees.Droid.Adapters;

namespace OfficeTools.Employees.Droid.Views
{
    [Activity(Theme = "@style/Theme.DesignDemo", Label = "NavigationDrawerActivity")]
    public class NavigationDrawerActivity : AppCompatActivity
    {
        private DrawerLayout mDrawerLayout;
        private ListView mDrawerList;

        protected RelativeLayout _completeLayout, _activityLayout;

        private List<NavigationDrawerItem> navDrawerItems;
        private NavigationListAdapter adapter;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Drawer);
        }

        public void set(String[] navMenuTitles)
        {
            mDrawerLayout = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
            mDrawerList = FindViewById<ListView>(Resource.Id.left_drawer);
            navDrawerItems = new List<NavigationDrawerItem>();
            for (int i = 0; i < navMenuTitles.Length; i++)
            {
                navDrawerItems.Add(new NavigationDrawerItem(navMenuTitles[i]));
                adapter = new NavigationListAdapter(Application.Context, navDrawerItems);
                mDrawerList.Adapter = adapter;
                mDrawerList.ItemClick += MDrawerList_ItemClick;
            }
        }

        private void MDrawerList_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            displayView(e.Position);
        }

        private void displayView(int position)
        {
            Intent i;
            switch (position)
            {
                case 0:
                    i = new Intent(this, typeof(HrmsActivity));
                    StartActivity(i);
                    Finish();
                    OverridePendingTransition(Resource.Animation.activity_open_translate, Resource.Animation.activity_close_scale);
                    break;
                case 1:
                    i = new Intent(this, typeof(HomePageActivity));
                    StartActivity(i);
                    Finish();
                    OverridePendingTransition(Resource.Animation.activity_open_translate, Resource.Animation.activity_close_scale);

                    break;
                case 2:
                    i = new Intent(this, typeof(HomePageActivity));
                    StartActivity(i);
                    Finish();
                    OverridePendingTransition(Resource.Animation.activity_open_translate, Resource.Animation.activity_close_scale);

                    break;
                case 3:
                    i = new Intent(this, typeof(HomePageActivity));
                    StartActivity(i);
                    Finish();
                    OverridePendingTransition(Resource.Animation.activity_open_translate, Resource.Animation.activity_close_scale);

                    break;

                case 4:
                    i = new Intent(this, typeof(HomePageActivity));
                    StartActivity(i);
                    Finish();
                    OverridePendingTransition(Resource.Animation.activity_open_translate, Resource.Animation.activity_close_scale);
                    break;
            }
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            if (item.ItemId == Android.Resource.Id.Home)
            {
                mDrawerLayout.OpenDrawer(Android.Support.V4.View.GravityCompat.Start);
                return true;
            }
            return base.OnOptionsItemSelected(item);
        }
    }

}
