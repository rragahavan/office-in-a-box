﻿using System.Collections.Generic;

namespace OfficeTools.Shared
{
    public class EmployeeListResponse
    {
        public bool success { get; set; }
        public List<Employee> data { get; set; }
    }
}
